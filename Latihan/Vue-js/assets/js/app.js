var app = new Vue({
    el: '#app',
    data: {
        show : false,
        message :'Hello World',
        message2 : 'Hello Vue',
        int: 1,
        int2: 2,
        result: null,
        kilometers: 0,
        meters: 0,
        textarea : '',
        select : [],
        checkbox : []
    },
    // Computed Properties
    computed: {
        sum: function () {
            return this.int + this.int2;
        }
    },
    // Method Properties
    methods: {
        sumProcess: function (int3) {
            return this.result = this.int + this.int2 + int3;
        }
    }, 
    // Watch Properties
    watch: {
        kilometers: function (val) {
            this.kilometers = val;
            this.meters = val * 1000;
        },
        meters: function (val) {
            this.kilometers = val / 1000;
            this.meters = val;
        }
    }
})