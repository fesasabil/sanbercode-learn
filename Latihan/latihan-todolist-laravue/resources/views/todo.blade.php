<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .completed {
            text-decoration: line-through;
        }
    </style>
</head>
<body>
    <div id="app">
        <h3>Todo List</h3>

        <input type="text" v-model="newTodo" @keyup.enter="addTodo">
        <ul>
            <li v-for="(todo, index) in todos">
                <span v-bind:class="{'completed' : todo.done}">@{{ todo.text }}</span>
                <button type="button" v-on:click="removeTodo(index, todo)">X</button>
                <button type="button" @click="toggleComplete(todo)">Done</button>
            </li>
        </ul>
    </div>    

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

    <script>
        new Vue({
            el : "#app",
            data : {
                newTodo : "",
                todos : []
            },
            methods : {
                addTodo : function () {
                    let textInput  = this.newTodo.trim();
                    if (textInput)
                        // POST /someUrl
                        this.$http.post('/api/todo/create', {text: textInput}).then(response => {
                            this.todos.unshift(
                                { text : textInput , done : 0}
                            )
                            this.newTodo = ''
                        });

                },
                removeTodo : function (index, todo) {
                    this.$http.post('/api/todo/delete/' + todo.id).then(response => {
                        this.todos.splice(index, 1)
                    });
                },
                toggleComplete : function (todo) {
                    this.$http.post('/api/todo/change-done-status/' + todo.id).then(response => {
                        todo.done = !todo.done
                    });
                }
            },
            mounted : function () {
                // GET /someUrl
                this.$http.get('/api/todo').then(response => {
                    let result = response.body.data;
                    this.todos = result
                });
            }
        })
    </script>
</body>
</html>