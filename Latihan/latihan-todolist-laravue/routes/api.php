<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('todo', 'TodoController@index');
Route::post('todo/create', 'TodoController@store');
Route::post('todo/change-done-status/{id}', 'TodoController@changeDoneStatus');
Route::post('todo/delete/{id}', 'TodoController@delete');