<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
// use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        User::create([
            'name'  => request('name'),
            'username' => request('username'),
            'email' => request('email'),
            'role_id' => request('role_id'),
            'password' => bcrypt(request('password'))
        ]);

        return response('Thanks, you are registered', 200);
    }
}
