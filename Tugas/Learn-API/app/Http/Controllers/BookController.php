<?php

namespace App\Http\Controllers;

use App\Http\Resources\BookCollection;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Requests\BookRequest;
use App\Http\Resources\BookResource;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookss = Book::paginate(5);
        return new BookCollection($bookss);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $books = Book::create([
            'code_buku' => request('code_buku'),
            'judul'     => request('judul'),
            'slug'      => \Str::slug(request('judul')),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('tahun_terbit')
        ]);

        return $books;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return new BookResource($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, Book $book)
    {
        $book = Book::update([
            'code_buku' => request('code_buku'),
            'judul'     => request('judul'),
            'slug'      => \Str::slug(request('judul')),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('tahun_terbit')
        ]);

        return new BookResource($book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return response()->json('Buku telah dihapus', 200);
    }
}
