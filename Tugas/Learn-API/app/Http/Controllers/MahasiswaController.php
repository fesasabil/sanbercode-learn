<?php

namespace App\Http\Controllers;

use App\Http\Resources\MahasiswaCollection;
use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use App\Http\Requests\MahasiswaRequest;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswa = Mahasiswa::paginate(5);
        return new MahasiswaCollection($mahasiswa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MahasiswaRequest $request)
    {
        $mahasiswas = auth()->user()->mahasiswa()->create([
            'nim' => request('nim'),
            'name' => request('name'),
            'fakultas'  => request('fakultas'),
            'jurusan' => request('jurusan'),
            'phone' => request('phone'),
            'wa'    => request('wa'),
            'user_id' => request('user_id')
        ]);

        return new MahasiswaCollection($mahasiswas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MahasiswaRequest $request, Mahasiswa $mahasiswa)
    {
        $mahasiswa = Mahasiswa::update([
            'nim' => request('nim'),
            'name' => request('name'),
            'fakultas'  => request('fakultas'),
            'jurusan' => request('jurusan'),
            'phone' => request('phone'),
            'wa'    => request('wa'),
            'user_id' => request('user_id')
        ]);

        return new MahasiswaCollection($mahasiswa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mahasiswa $mahasiswa)
    {
        $mahasiswa->delete();

        return response()->json('Mahasiswa telah dihapus', 200);
    }
}
