<?php

namespace App\Http\Controllers;

use App\Http\Resources\LoanCollection;
use Illuminate\Http\Request;
use App\Models\Loan;
use App\Http\Requests\LoanRequest;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loan = Loan::paginate(5);
        return new LoanCollection($loan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoanRequest $request)
    {

        $peminjamans = Loan::create([
            'waktu_peminjaman' => request('waktu_peminjaman'),
            'batas_waktu_peminjaman' => request('batas_waktu_peminjaman'),
            'waktu_pengembalian'  => request('fakultas'),
            'book_id' => request('book_id'),
            'mahasiswa_id' => request('mahasiswa_id'),
            'status'    => request('status'),
        ]);

        return new LoanCollection($peminjamans);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LoanRequest $request, Loan $loan)
    {
        $loan = Loan::update([
            'waktu_peminjaman' => request('waktu_peminjaman'),
            'batas_waktu_peminjaman' => request('batas_waktu_peminjaman'),
            'waktu_pengembalian'  => request('fakultas'),
            'book_id' => request('book_id'),
            'mahasiswa_id' => request('mahasiswa_id'),
            'status'    => request('status'),
        ]);

        return new LoanCollection($loan);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        $loan->delete();

        return response()->json('Peminjaman telah dihapus', 200);
    }
}
