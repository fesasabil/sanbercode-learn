<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'code_buku', 'judul', 'pengarang', 'tahun_terbit', 'slug'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function pinjaman()
    {
        return $this->hasMany(Pinjaman::class);
    }
}
