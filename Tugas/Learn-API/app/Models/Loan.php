<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Mahasiswa;
use App\Models\Book;

class Loan extends Model
{
    protected $fillable = [
        'waktu_peminjaman', 'batas_waktu_peminjaman', 'waktu_pengembalian', 'mahasiswa_id', 'book_id', 'status'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
