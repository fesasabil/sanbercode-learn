<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Mahasiswa extends Model
{
    protected $fillable = [
        'nim', 'name', 'fakultas', 'jurusan', 'phone', 'wa', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
