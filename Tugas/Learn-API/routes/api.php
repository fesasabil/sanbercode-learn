<?php

use App\Http\Middleware\checkRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Auth')->group(function() {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::middleware(['auth:api', 'checkRole:1'])->group(function() {
    Route::post('create-book', 'BookController@store');
    Route::patch('update-books/{book}', 'BookController@update');
    Route::delete('delete-books/{book}', 'BookController@delete');

    Route::post('create-mahasiswa', 'MahasiswaController@store');
    Route::patch('update-mahasiswas/{mahasiswa}', 'MahasiswaController@update');
    Route::delete('delete-mahasiswas/{mahasiswa}', 'MahasiswaController@delete');

    Route::post('peminjaman-buku', 'PinjamanController@store');
    Route::patch('update-peminjaman-buku/{loan}', 'PinjamanController@update');
    Route::delete('delete-peminjaman/{loan}', 'PinjamanController@delete');
});


Route::middleware(['auth:api', 'checkRole:1, 2'])->group(function() {
    Route::get('books', 'BookController@index');
    Route::get('books/{book}', 'BookController@show');
    Route::get('mahasiswas', 'MahasiswaController@index');
    Route::get('loans', 'PinjamanController@index');
});



