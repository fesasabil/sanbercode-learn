/**
 * 1. Mengubah fungsi menjadi fungsi arrow
 */
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()

// ES6
const golden = () => {
    return console.log("this is golden")
}

golden()


/**
 * 2. Sederhanakan menjadi Object literal di ES6 
 */
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()

// ES6
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(firstName + " " + lastName)
        }
    }
}

newFunction("Dr.", "William").fullName()


/**
 * 3. Destructuring
 */
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// ES6
const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation, spell);


/**
 * 4. Array Spreading
 */
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)

// ES6
const combinedd = [...west, ...east]
console.log(combinedd)


/**
 * 5. Template Literals
 */
const planet = "earth"
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' +       'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +     'incididunt ut labore et dolore magna aliqua. Ut enim' +     ' ad minim veniam'

// Driver Code console.log(before) 

// ES6
var after = `Lorem ${ planet } dolor sit amet, ` +       `consectetur adipiscing elit, ${ view } do eiusmod tempor ` +     'incididunt ut labore et dolore magna aliqua. Ut enim' +     ' ad minim veniam'