<?php

namespace App\Http\Controllers;

use App\Http\Resources\InventarisResource;
use App\Inventaris;
use Illuminate\Http\Request;

class InventarisController extends Controller
{
    public function index ()
    {
        $inventaris = Inventaris::orderBy('created_at', 'desc')->paginate(10);

        return InventarisResource::collection($inventaris);
    }

    public function show ()
    {
        // $inventaris = Inventaris::orderBy('created_at', 'desc')->paginate(10);

        return view('inventaris');
    }

    public function store(Request $request)
    {
        $inven = Inventaris::create([
            'name' => $request->name,
            'status' => $request->status
        ]);

        return new InventarisResource($inven);
    }

    public function update(Request $request, $id)
    {
        $inven = Inventaris::findOrFail($id);
        $inven->update([
            'name'   => $request->name,
            'status' => $request->status
        ]);
        return new InventarisResource($inven);
    }

    public function delete($id)
    {
        Inventaris::destroy($id);
        return response()->json('Berhasil terhapus', 200);
    }

    public function edit($id)
    {
        $inven = Inventaris::findOrFail($id);

        // return view('inventaris.edit');
        return new InventarisResource($inven);
    }
}
