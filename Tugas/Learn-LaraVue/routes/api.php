<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('inventaris', 'InventarisController@index');
Route::post('inventaris/create', 'InventarisController@store');
Route::get('inventaris/edit/{id}', 'InventarisController@edit');
Route::post('inventaris/update/{id}', 'InventarisController@update');
Route::post('inventaris/delete/{id}', 'InventarisController@delete');
