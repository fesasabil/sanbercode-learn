<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('role');
    // }

    public function isAdmin()
    {
        return view('admin');
    }

    public function superAdmin()
    {
        return view('superAdmin');
    }

    public function guest()
    {
        return view('guest');
    }
}
