<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $minRole = 0)
    {
        // if(Auth::user()->superAdmin()){
        //     return $next($request);
        // }
        // else if(Auth::user()->isAdmin()){
        //     return $next($request);
        // }

        // if(in_array($request->user()->role,$roles)) {
        //     return $next($request);
        // }
        // abort(403);

        if(Auth::user()->isAuthorized($minRole)) {
            return $next($request);
        }
        abort(403);
    }

}
