<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware(['auth', 'Checkrole'])->group(function() {
//     Route::get('/superAdmin', 'AdminController@superAdmin')->name('superAdmin');
//     Route::get('/admin', 'AdminController@isAdmin')->name('admin');
// });

// Route::middleware(['auth'])->group(function() {
//     Route::get('/guest', 'AdminController@guest')->name('guest');
// });

// Route::group(['middleware' => ['auth', 'checkRole:superAdmin']], function() {
//     Route::get('/superAdmin', 'AdminController@superAdmin')->name('superAdmin');
//     Route::get('/admin', 'AdminController@isAdmin')->name('admin');
//     Route::get('/guest', 'AdminController@guest')->name('guest');   
// });

// Route::group(['middleware' => ['auth', 'checkRole:admin']], function() {
//     Route::get('/admin', 'AdminController@isAdmin')->name('admin');
//     Route::get('/guest', 'AdminController@guest')->name('guest');
// });

// Route::group(['middleware' => ['auth', 'checkRole:guest']], function() {
//     Route::get('/guest', 'AdminController@guest')->name('guest');
// });

Route::middleware('auth')->group(function() {
    Route::get('/superAdmin', 'AdminController@superAdmin')->name('superAdmin')->middleware('checkRole:2');
    Route::get('/admin', 'AdminController@isAdmin')->name('admin')->middleware('checkRole');
    Route::get('/guest', 'AdminController@guest')->name('guest')->middleware('checkRole');
});

