<?php

// class hewan
abstract class Animals
{
    // Property
    protected $names,
              $darah = 50,
              $jumlahKaki,
              $keahlian;

    // Konstruktor
    public function __construct($names)
    {
        $this->names = $names;
    }

    abstract public function getinfoAnimals();

    // Method
    public function atraksi()
    {
        $str = $this->names . " sedang " . $this->keahlian;
        return $str;
    }

    public function getinfo()
    {
        $str = "Nama : $this->names <br>" .
               "Darah: $this->darah <br>" .
               "Jumlah Kaki: $this->jumlahKaki <br>" .
               "Keahlian: $this->keahlian";
        return $str;
    }
}

?>