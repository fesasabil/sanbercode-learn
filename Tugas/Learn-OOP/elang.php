<?php

class Elang extends Animals
{
    use Fight;

    public function __construct($names)
    {
        parent::__construct($names);
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getinfoAnimals()
    {
        $str = "==== Elang ==== <br>" .
               parent::getinfo() . "<br>" .
               "Attack power : $this->attackPower <br>" .
               "Defence power : $this->defencePower <br>";
        return $str;
    }
}

?>