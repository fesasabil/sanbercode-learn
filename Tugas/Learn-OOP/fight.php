<?php

trait Fight
{
    public $attackPower, $defencePower;

    public function serang($animals)
    {
        echo "$this->names sedang menyerang $animals->names <br>";

        $animals->diserang($this);
    }

    public function diserang($animals)
    {
        echo "$this->names sedang diserang $animals->names <br>";

        $this->darah = $this->darah - ($animals->attackPower / $this->defencePower);

        echo "Darah $this->names tersisa : $this->darah <br>";
    }

    
}

?>