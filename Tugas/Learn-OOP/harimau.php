<?php

class Harimau extends Animals
{
    use Fight;

    public function __construct($names)
    {
        parent::__construct($names);
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getinfoAnimals()
    {
        $str = "==== Harimau ==== <br>" .
               parent::getinfo() . "<br>" .
               "Attack power : $this->attackPower <br>" .
               "Defence power : $this->defencePower <br>";
        return $str;
    }
}

?>