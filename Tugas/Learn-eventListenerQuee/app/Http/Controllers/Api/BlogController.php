<?php

namespace App\Http\Controllers\Api;

use App\Events\ReviewBlogEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Http\Resources\BlogResource;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::paginate(5);
        return BlogResource::collection($blogs);
    }

    public function store(BlogRequest $request)
    {
        $blog = auth()->user()->create($this->blogStore());

        event(new ReviewBlogEvent($blog));
        return new BlogResource($blog);
    }

    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $blog->update($this->blogStore());
    }

    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        $blog->delete();

        return response()->json('Blog telah dihapus', 200);
    }

    public function blogStore()
    {
        return [
            'name'   => request('name'),
            'judul'  => request('judul'),
            'isi'    => request('isi'),
            'slug'   => \Str::slug(request('judul')),
            'user_id' => request('user_id')
        ];
    }
}
