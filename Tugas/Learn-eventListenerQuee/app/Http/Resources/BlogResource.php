<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'  => $this->name,
            'judul' => $this->judul,
            'slug'  => $this->slug,
            'isi'   => $this->isi,
            'published' => $this->created_at->format('d F Y'),
        ];
    }
}
