<?php

namespace App\Listeners;

use App\Events\EditorReview;
use App\Mail\ReviewEditorMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendNotificationEditor
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EditorReview  $event
     * @return void
     */
    public function handle(EditorReview $event)
    {
        Mail::to($event->user)->send(new ReviewEditorMail($event->user));
    }
}
