<?php

namespace App\Listeners;

use App\Events\PublishBlogEvent;
use App\Mail\PublishBlogMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendNotificationPublishBlog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PublishBlogEvent  $event
     * @return void
     */
    public function handle(PublishBlogEvent $event)
    {
        Mail::to($event->blog)->send(new PublishBlogMail($event->blog));
    }
}
