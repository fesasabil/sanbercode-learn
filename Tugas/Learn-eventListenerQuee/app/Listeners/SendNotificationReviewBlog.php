<?php

namespace App\Listeners;

use App\Events\ReviewBlogEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendNotificationReviewBlog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReviewBlogEvent  $event
     * @return void
     */
    public function handle(ReviewBlogEvent $event)
    {
        Mail::to($event->user)->send(new ReviewBlogMail($event->user));
    }
}
