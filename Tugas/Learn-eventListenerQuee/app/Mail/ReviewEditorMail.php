<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Blog;


class ReviewEditorMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user,
                $blog;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Blog $blog)
    {
        $this->user = $user;
        $this->blog = $blog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
                    ->view('ReviewBlogEditor')
                    ->with([
                        'name'  => $this->user->name,
                        'judul' => $this->blog->judul
                    ]);
    }
}
