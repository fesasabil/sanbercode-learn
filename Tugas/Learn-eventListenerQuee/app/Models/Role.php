<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Route;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = [];

    public function routes()
    {
        return $this->belongsToMany(Route::class, 'role_route', 'role_id', 'route_id');
    }
}
